#include "substring_search.h"

String::String(std::string _pattern, std::string _str) {
  pattern = _pattern;
  str = _str;
  size = str.size();
}

void String::countZFunction(std::string& zFunctionString) {
  const size_t zFunctionStrSize = zFunctionString.size();
  zFunctionArray.assign(zFunctionString.size(), 0);
  zFunctionArray.resize(zFunctionStrSize);
  if (zFunctionStrSize == 0)
    return;
  zFunctionArray[0] = zFunctionStrSize;

  int leftPointer = 0;
  int rightPointer = 1;
  for (size_t index = 1; index < zFunctionStrSize; ++index) {
    if (index >= rightPointer) {
	  while (index + zFunctionArray[index] < zFunctionStrSize &&
          zFunctionString[index + zFunctionArray[index]] == zFunctionString[zFunctionArray[index]])
	    ++zFunctionArray[index];
	  rightPointer = index + zFunctionArray[index];
	  leftPointer = index;
    }
    else {
	  if (zFunctionArray[index - leftPointer] < rightPointer - index)
	    zFunctionArray[index] = zFunctionArray[index - leftPointer];
	  else {
		zFunctionArray[index] = 0;
		while (rightPointer + zFunctionArray[index] < zFunctionStrSize &&
		    zFunctionString[rightPointer - index + zFunctionArray[index]] ==
		    zFunctionString[rightPointer + zFunctionArray[index]])
		  ++zFunctionArray[index];
		rightPointer += zFunctionArray[index];
		zFunctionArray[index] = rightPointer - index;
		leftPointer = index;
	  }
    }
  }
}

void String::getSubstringsStartIndex() {
  countZFunction(str);
  countZFunction(pattern + "#" + str);
  for (int index = pattern.size(); index < size + pattern.size() + 1; ++index) {
	if (zFunctionArray[index] == pattern.size()) {
      substringsBeginnings.push_back(index - pattern.size() - 1);
	}
  }
}

void String::printSubstringsStartIndex() {
  for (int index = 0; index < substringsBeginnings.size(); ++index) {
    std::cout << substringsBeginnings[index] << " ";
  }
}