#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

class String {
public:
  String(std::string _pattern, std::string _str);

  void countZFunction(std::string& zFunctionString);

  void getSubstringsStartIndex();

  void printSubstringsStartIndex();

  std::vector <int> substringsBeginnings;
private:
  int size;
  std::string pattern;
  std::string str;
  std::vector <int> zFunctionArray;
};