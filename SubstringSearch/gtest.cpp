#include "substring_search.h"
#include <gtest\gtest.h>
#include <string>

TEST(TestCase, Test1)
{
  std::string pattern = "a";
  std::string str = "aaaaa";
  String stringClass(pattern, str);
  stringClass.getSubstringsStartIndex();
  std::vector <int> actual = { 0, 1, 2, 3, 4 };
  std::vector <int> answer = stringClass.substringsBeginnings;
  for (int index = 0; index < stringClass.substringsBeginnings.size(); ++index) {
    ASSERT_EQ(answer[index], actual[index]);
  }
}

TEST(TestCase, Test2)
{
  std::string pattern = "aba";
  std::string str = "abacababa";
  String stringClass(pattern, str);
  stringClass.getSubstringsStartIndex();
  std::vector <int> actual = { 0, 4, 6 };
  std::vector <int> answer = stringClass.substringsBeginnings;
  for (int index = 0; index < stringClass.substringsBeginnings.size(); ++index) {
    ASSERT_EQ(answer[index], actual[index]);
  }
}

int main(int argc, char** argv)
{
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
