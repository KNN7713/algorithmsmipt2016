#include "substring_search.h"

int main() {
  std::string pattern;
  std::string str;
  std::cin >> pattern;
  std::cin >> str;
  String string(pattern, str);
  string.getSubstringsStartIndex();
  string.printSubstringsStartIndex();
  return 0;
}