#include "string_from_zfunction.h"

int main() {
  int curNumber;
  std::vector <int> zFunctionArray;
  while (std::cin >> curNumber) {
    zFunctionArray.push_back(curNumber);
  }
  String str("");
  str.buildStringFromZFunction(zFunctionArray);
  str.printString();
  return 0;
}