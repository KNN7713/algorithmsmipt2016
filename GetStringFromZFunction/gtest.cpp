#include "string_from_zfunction.h"
#include <gtest\gtest.h>
#include <string>

TEST(TestCase, Test1)
{
  std::vector <int> zFunctionArray = { 5, 3, 2, 1, 0 };
  String str("");
  str.buildStringFromZFunction(zFunctionArray);
  ASSERT_EQ(str.str, "aaaab");
}


TEST(TestCase, Test2)
{
  std::vector <int> zFunctionArray = { 0, 0, 0, 0, 0, 0 };
  String str("");
  str.buildStringFromZFunction(zFunctionArray);
  ASSERT_EQ(str.str, "abbbbb");
}

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}