#include "string_from_zfunction.h"

String::String(std::string _str) {
  str = _str;
  size = str.size();
}

void String::countPrefixFunction() {
  prefixFunctionArray.assign(size, 0);
  prefixFunctionArray.resize(size);
  for (int index = 1; index < size; ++index) {
    int cur = prefixFunctionArray[index - 1];
    while (cur > 0 && str[index] != str[cur]) {
	  cur = prefixFunctionArray[cur - 1];
    }
    if (str[index] == str[cur]) {
	  ++cur;
    }
    prefixFunctionArray[index] = cur;
  }
}

void String::buildZFunctionFromPrefixFunction() {
  zFunctionArray.assign(prefixFunctionArray.size(), 0);
  zFunctionArray.resize(prefixFunctionArray.size());
  for (int index = 1; index < prefixFunctionArray.size(); ++index) {
    if (prefixFunctionArray[index] > 0) {
	  zFunctionArray[index - prefixFunctionArray[index] + 1] = prefixFunctionArray[index];
    }
  }
  zFunctionArray[0] = prefixFunctionArray.size();
  int index = 1;
  while (index < prefixFunctionArray.size()) {
    int temp = index;
    if (zFunctionArray[index] > 0) {
      for (int secondIndex = 1; secondIndex < zFunctionArray.size(); ++secondIndex) {
        if (zFunctionArray[index + secondIndex] > zFunctionArray[secondIndex]) {
          break;
        }
        zFunctionArray[index + secondIndex] = std::min(zFunctionArray[secondIndex], zFunctionArray[index] - secondIndex);
        temp = index + secondIndex;
      }
      index = temp + 1;
    }
  }
}

void String::buildPrefixFunctionFromZFunction() {
  prefixFunctionArray.assign(zFunctionArray.size(), 0);
  prefixFunctionArray.resize(zFunctionArray.size());
  for (int index = 1; index < zFunctionArray.size(); ++index) {
	for (int j = zFunctionArray[index] - 1; j >= 0; --j) {
	  if (prefixFunctionArray[index + j] > 0) {
        break;
	  }
	  else {
        prefixFunctionArray[index + j] = j + 1;
	  }
	}
  }
}

void String::buildStringFromPrefixFunction(const std::vector<int>& prefix_function_array) {
  prefixFunctionArray = prefix_function_array;
  str = "a";
  for (int index = 1; index < prefixFunctionArray.size(); ++index) {
    if (prefixFunctionArray[index] == 0 && prefixFunctionArray[index - 1] == 0) {
      str += 'b';
    }
    if (prefixFunctionArray[index] != 0) {
      str += str[prefixFunctionArray[index] - 1];
    }
    bool isEqual;
    if (prefixFunctionArray[index] == 0 && prefixFunctionArray[index - 1] != 0) {
      int position = prefixFunctionArray[index - 1];
      std::string unorrectSymbol = "";
      while (position >= 1) {
        isEqual = true;
        for (int step = 0; step < position; ++step) {
          if (str[step] != str[index - position + step]) {
            isEqual = false;
          }
        }
        if (isEqual) {
          unorrectSymbol += str[position];
        }
        --position;
      }
      for (char curChar = 'b'; curChar <= 'z'; ++curChar) {
        if (unorrectSymbol.find(curChar) != std::string::npos) {
          continue;
        }
        str = str + curChar;
        break;
      }
    }
  }
}

void String::buildStringFromZFunction(const std::vector<int>& z_function_array) {
	zFunctionArray = z_function_array;
	buildPrefixFunctionFromZFunction();
	buildStringFromPrefixFunction(prefixFunctionArray);
}

void String::countZFunction(std::string& zFunctionString) {
  const size_t zFunctionStrSize = zFunctionString.size();
  zFunctionArray.assign(zFunctionString.size(), 0);
  zFunctionArray.resize(zFunctionStrSize);
  if (zFunctionStrSize == 0)
    return;
  zFunctionArray[0] = zFunctionStrSize;

  int leftPointer = 0;
  int rightPointer = 1;
  for (size_t index = 1; index < zFunctionStrSize; ++index) {
	if (index >= rightPointer) {
	  while (index + zFunctionArray[index] < zFunctionStrSize &&
	      zFunctionString[index + zFunctionArray[index]] == zFunctionString[zFunctionArray[index]])
	    ++zFunctionArray[index];
	  rightPointer = index + zFunctionArray[index];
	  leftPointer = index;
	}
	else {
		if (zFunctionArray[index - leftPointer] < rightPointer - index)
		  zFunctionArray[index] = zFunctionArray[index - leftPointer];
		else {
		  zFunctionArray[index] = 0;
		  while (rightPointer + zFunctionArray[index] < zFunctionStrSize &&
		      zFunctionString[rightPointer - index + zFunctionArray[index]] ==
			  zFunctionString[rightPointer + zFunctionArray[index]])
		    ++zFunctionArray[index];
		  rightPointer += zFunctionArray[index];
		  zFunctionArray[index] = rightPointer - index;
		  leftPointer = index;
		}
	}
  }
}

void String::getSubstringsStartIndex() {
  countZFunction(str);
  countZFunction(pattern + "#" + str);
  for (int index = pattern.size(); index < size + pattern.size() + 1; ++index) {
	if (zFunctionArray[index] == pattern.size()) {
      substringsBeginnings.push_back(index - pattern.size() - 1);
	}
  }
}

void String::printSubstringsStartIndex() {
  for (int index = 0; index < substringsBeginnings.size(); ++index) {
    std::cout << substringsBeginnings[index] << " ";
  }
}

void String::printString() {
  std::cout << str;
}
