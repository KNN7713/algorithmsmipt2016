#include "string_from_prefix_function.h"

TEST(TestCase, Test1) {
  std::vector <int> prefixFunctionArray = { 0, 0, 1, 0, 1, 2, 3, 1 };
  String str("");
  str.buildStringFromPrefixFunction(prefixFunctionArray);
  ASSERT_EQ(str.str, "abacabaa");
}

TEST(TestCase, Test2) {
  std::vector <int> prefixFunctionArray = { 0, 1, 0, 1, 2, 0, 0, 0, 1 };
  String str("");
  str.buildStringFromPrefixFunction(prefixFunctionArray);
  ASSERT_EQ(str.str, "aabaacbba");
}

int main(int argc, char** argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
