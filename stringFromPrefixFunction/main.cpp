#include "string_from_prefix_function.h"

int main() {
  int curNumber;
  std::vector <int> prefixFunctionArray;
  while (std::cin >> curNumber) {
    prefixFunctionArray.push_back(curNumber);
  }
  String str("");
  str.buildStringFromPrefixFunction(prefixFunctionArray);
  str.printString();
  return 0;
}