#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

class String {
public:
  String(std::string _str);

  void countPrefixFunction();

  void buildPrefixFunctionFromZFunction();

  void buildStringFromPrefixFunction(const std::vector<int>& prefix_function_array);

  void countZFunction(std::string& zFunctionString);

  void getSubstringsStartIndex();

  void printSubstringsStartIndex();

  void printString();

  std::string str;

private:
  int size;
  std::string pattern;
  std::vector <int> zFunctionArray;
  std::vector <int> prefixFunctionArray;
  std::vector <int> substringsBeginnings;
};