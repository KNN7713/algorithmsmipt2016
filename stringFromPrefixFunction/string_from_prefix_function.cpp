#include "string_from_prefix_function.h"

String::String(std::string _str) {
  str = _str;
  size = str.size();
}

void String::countPrefixFunction() {
  prefixFunctionArray.assign(size, 0);
  prefixFunctionArray.resize(size);
  for (int index = 1; index < size; ++index) {
    int cur = prefixFunctionArray[index - 1];
    while (cur > 0 && str[index] != str[cur]) {
      cur = prefixFunctionArray[cur - 1];
    }
    if (str[index] == str[cur]) {
      ++cur;
    }
    prefixFunctionArray[index] = cur;
  }

  for (int i = 0; i < size; ++i) {
    std::cout << prefixFunctionArray[i] << " ";
  }
}

void String::buildPrefixFunctionFromZFunction() {
  prefixFunctionArray.assign(size, 0);
  prefixFunctionArray.resize(size);
  for (int index = 0; index < size; ++index) {
    for (int j = zFunctionArray[index] - 1; j >= 0; --j) {
      if (prefixFunctionArray[index + j] > 0) {
        break;
      }
      else {
        prefixFunctionArray[index + j] = j + 1;
      }
    }
  }
}

void String::buildStringFromPrefixFunction(const std::vector<int>& prefix_function_array) {
  prefixFunctionArray = prefix_function_array;
  str = "a";
  for (int index = 1; index < prefixFunctionArray.size(); ++index) {
    if (prefixFunctionArray[index] == 0 && prefixFunctionArray[index - 1] == 0) {
      str += 'b';
    }
    if (prefixFunctionArray[index] != 0) {
      str += str[prefixFunctionArray[index] - 1];
    }
    bool isEqual;
    if (prefixFunctionArray[index] == 0 && prefixFunctionArray[index - 1] != 0) {
      int position = prefixFunctionArray[index - 1];
      std::string unorrectSymbol = "";
      while (position >= 1) {
        isEqual = true;
        for (int step = 0; step < position; ++step) {
          if (str[step] != str[index - position + step]) {
            isEqual = false;
          }
        }
        if (isEqual) {
          unorrectSymbol += str[position];
        }
        --position;
      }
      for (char curChar = 'b'; curChar <= 'z'; ++curChar) {
        if (unorrectSymbol.find(curChar) != std::string::npos) {
          continue;
        }
        str = str + curChar;
        break;
      }
    }
  }
}

void String::countZFunction(std::string& zFunctionString) {
  const size_t zFunctionStrSize = zFunctionString.size();
  zFunctionArray.assign(zFunctionString.size(), 0);
  zFunctionArray.resize(zFunctionStrSize);
  if (zFunctionStrSize == 0)
    return;
  zFunctionArray[0] = zFunctionStrSize;

  int leftPointer = 0;
  int rightPointer = 1;
  for (size_t index = 1; index < zFunctionStrSize; ++index) {
    if (index >= rightPointer) {
      while (index + zFunctionArray[index] < zFunctionStrSize &&
        zFunctionString[index + zFunctionArray[index]] == zFunctionString[zFunctionArray[index]])
        ++zFunctionArray[index];
        rightPointer = index + zFunctionArray[index];
        leftPointer = index;
      }
      else {
        if (zFunctionArray[index - leftPointer] < rightPointer - index)
          zFunctionArray[index] = zFunctionArray[index - leftPointer];
      else {
        zFunctionArray[index] = 0;
        while (rightPointer + zFunctionArray[index] < zFunctionStrSize &&
            zFunctionString[rightPointer - index + zFunctionArray[index]] ==
            zFunctionString[rightPointer + zFunctionArray[index]])
          ++zFunctionArray[index];
        rightPointer += zFunctionArray[index];
        zFunctionArray[index] = rightPointer - index;
        leftPointer = index;
      }
    }
  }
}

void String::getSubstringsStartIndex() {
  countZFunction(str);
  countZFunction(pattern + "#" + str);
  for (int index = pattern.size(); index < size + pattern.size() + 1; ++index) {
    if (zFunctionArray[index] == pattern.size()) {
    substringsBeginnings.push_back(index - pattern.size() - 1);
    }
  }
}

void String::printSubstringsStartIndex() {
  for (int index = 0; index < substringsBeginnings.size(); ++index) {
    std::cout << substringsBeginnings[index] << " ";
  }
}

void String::printString() {
  std::cout << str;
}