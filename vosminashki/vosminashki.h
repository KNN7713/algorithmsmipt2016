#include <iostream>
#include <queue>
#include <set>
#include <map>
#include <stdlib.h>
#include <cstdio>
#include <vector>

class State {
public:
  State(short n, std::vector<short> data);

  State(short n, std::vector<std::vector<short>> data);

  struct comparator {
    bool operator()(const State& first, const State& second) const;
  };


  bool operator < (const State& another) const;

  struct comp {
    bool operator() (State* x, State* y);
  };

  std::string run(State &state);

private:
  int heuristic;
  short zeroX;
  short zeroY;
  short size;
  char course;
  State* parent;
  std::vector<std::vector<short>> field;
  
  int getManhattanHeuristic();

  bool checkSolutionExist(State &state);
};
