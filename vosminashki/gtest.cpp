#include "vosminashki.h"
#include <gtest\gtest.h>
#include <string>

TEST(TestCase, Test1)
{
  short cellNumber = 3;
  std::string answer;
  std::vector<short> data = { 0, 1, 6, 4, 3, 2, 7, 5, 8 };
  State state(cellNumber, data);
  answer = state.run(state);
  
  if (answer.size() > 0) {
    actual = answer.size() - 1;
  }
  else {
    actual = -1;
  }
  ASSERT_EQ(actual, 8);
  std::string actual = "RDRULDDR";
  for (int index = 0; index < answer.size(); ++index) {
    ASSERT_EQ(answer[index], actual[index]);
  }
}

TEST(TestCase, Test2)
{
  short cellNumber = 3;
  std::string answer;
  std::vector<short> data = { 1, 2, 3, 8, 0, 4, 7, 6, 5 };
  State state(cellNumber, data);
  answer = state.run(state);
  int actual = 0;
  if (answer.size() > 0) {
    actual = answer.size() - 1;
  }
  else {
    actual = -1;
  }
  ASSERT_EQ(actual, -1);
}


int main(int argc, char** argv)
{
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}