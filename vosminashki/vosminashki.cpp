#include "vosminashki.h"

State::State(short n, std::vector<short> data) : size(n), field(n, std::vector<short>(n, -1)), heuristic(0), parent(nullptr) {
  for (short index = 0; index < n * n; ++index) {
    field[index / size][index % size] = data[index];
    if (data[index] == 0) {
      zeroX = index % size;
      zeroY = index / size;
    }
  }
}

State::State(short n, std::vector<std::vector<short>> data) : size(n), heuristic(0), parent(nullptr), field(data) {
  for (short firstCoord = 0; firstCoord < n; ++firstCoord) {
    for (short secondCoord = 0; secondCoord < n; ++secondCoord) {
      if (field[firstCoord][secondCoord] == 0) {
        zeroY = firstCoord;
        zeroX = secondCoord;
      }
    }
  }
}

struct State::comparator {
  bool operator()(const State& first, const State& second) const {
    if (first.heuristic < second.heuristic) {
      return true;
    }
    return first.field < second.field;
  }
};


bool State::operator < (const State& another) const {
  if (this->heuristic < another.heuristic) {
    return true;
  }
  return this->field < another.field;
}

struct comp {
  bool operator() (State* x, State* y) {
    return (*y) < (*x);
  }
};

std::string State::run(State &state) {
  std::string result;
  result.clear();

  if (!checkSolutionExist(state)) {
    std::cout << "-1";
    return result;
  }

  std::priority_queue<State*, std::vector<State*>, comp> queue;
  std::vector<State*> positions;
  positions.push_back(&state);
  queue.push(&state);
  std::map<State, int, comparator> usedHeuristic;
  state.heuristic = state.getManhattanHeuristic();
  usedHeuristic.insert(usedHeuristic.begin(), std::pair<State, int>(state, state.heuristic));

  while (!queue.empty()) {
    State* current = queue.top();
    queue.pop();
    if (current->heuristic == 0) {
      while (current != nullptr) {
        result = current->course + result;
        current = current->parent;
      }
      return result;
    }
    for (int i = -1; i <= 1; ++i) {
      for (int j = -1; j <= 1; ++j) {
        int newx = current->zeroX + i;
        int newy = current->zeroY + j;
        if (newx >= 0 && newx < state.size && newy >= 0 && newy < state.size &&
          !(i == 0 && j == 0) && !(abs(i) == 1 && abs(j) == 1)) {
          std::vector<std::vector<short>> new_field(current->field);
          std::swap(new_field[current->zeroY][current->zeroX], new_field[newy][newx]);
          State* new_state = new State(state.size, new_field);
          int new_heuristic = new_state->getManhattanHeuristic();
          if (usedHeuristic.find(*new_state) == usedHeuristic.end()) {
            usedHeuristic.insert(usedHeuristic.begin(), std::pair<State, int>(*new_state, new_heuristic));
            new_state->parent = current;
            new_state->heuristic = new_heuristic;
            if (i == -1)
              new_state->course = 'L';
            if (i == 1)
              new_state->course = 'R';
            if (j == 1)
              new_state->course = 'D';
            if (j == -1)
              new_state->course = 'U';
            positions.push_back(new_state);
            queue.push(new_state);
          }
        }
      }
    }
  }
  for (size_t index = 1; index < positions.size(); index++) {
    delete positions[index];
  }
  return result;
}

int State::getManhattanHeuristic() {
  int res = 0;
  for (int firstCoord = 0; firstCoord < size; ++firstCoord) {
    for (int secondCoord = 0; secondCoord < size; ++secondCoord) {
      if (field[firstCoord][secondCoord] != 0) {
        res += (abs(firstCoord - (field[firstCoord][secondCoord] - 1) / size) + abs(secondCoord - (field[firstCoord][secondCoord] - 1) % size));
      }
    }
  }
  return res;
}

bool State::checkSolutionExist(State &state) {
  int invariant = 0;
  std::vector<short> data(state.size * state.size, -1);
  for (short firstCoord = 0; firstCoord < state.size; firstCoord++) {
    for (short secondCoord = 0; secondCoord < state.size; ++secondCoord) {
      data[firstCoord * state.size + secondCoord] = state.field[firstCoord][secondCoord];
    }
  }

  for (short i = 0; i < data.size(); i++) {
    if (data[i]) {
      for (short j = 0; j < i; j++) {
        if (data[i] < data[j]) {
          ++invariant;
        }
      }
    }
    else {
      invariant += 1 + i / state.size;
    }
  }
  return (invariant + state.size) % 2 == 0;
}