#include "vosminashki.h"

int main() {
  freopen("puzzle.in", "r", stdin);
  freopen("puzzle.out", "w", stdout);
  short cellNumber = 3;
  std::string answer;
  std::vector<short> data;
  for (int step = 0; step < cellNumber * cellNumber; ++step) {
    short val;
    std::cin >> val;
    data.push_back(val);
  }
  State state(cellNumber, data);
  answer = state.run(state);
  if (answer.size() > 0) {
    std::cout << answer.size() - 1 << "\n";
  }
  else {
    std::cout << "-1";
  }
  for (int index = 0; index < answer.size(); ++index) {
    std::cout << answer[index];
  }
  fclose(stdin);
  fclose(stdout);
  return 0;
}