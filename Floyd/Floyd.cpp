#include <fstream>
#include <iostream>
#include <vector>
#include <gtest\gtest.h>

template <class T>
class Graph {
public:

	Graph() {
		size = 0;
	};

	Graph(size_t _size) : size(_size) {
		matrix.resize(size);
		distance.resize(size);

		for (size_t row = 0; row < size; ++row) {
			matrix[row].resize(size, 0);
			distance[row].resize(size, 0);
		}
	};

	void Floyd() {

		distance = matrix;

		for (size_t vertex_k = 0; vertex_k < size; ++vertex_k) {
			for (size_t vertex_i = 0; vertex_i < size; ++vertex_i) {
				for (size_t vertex_j = 0; vertex_j < size; ++vertex_j) {
					if (distance[vertex_i][vertex_j] > distance[vertex_i][vertex_k] + distance[vertex_k][vertex_j]) {
						distance[vertex_i][vertex_j] = distance[vertex_i][vertex_k] + distance[vertex_k][vertex_j];
					}
				}
			}
		}
	};

	void GetMatrix(std::ifstream& in) {
		for (size_t row = 0; row < size; ++row) {
			for (size_t col = 0; col < size; ++col) {
				in >> matrix[row][col];
			}
		}
	}

	void PrintMatrix(std::ofstream& out) {
		for (size_t row = 0; row < size; ++row) {
			for (size_t col = 0; col < size; ++col) {
				out << matrix[row][col] << " ";
			}
			out << "\n";
		}
	}

	void PrintDistance(std::ofstream& out) {
		for (size_t row = 0; row < size; ++row) {
			for (size_t col = 0; col < size; ++col) {
				out << distance[row][col] << " ";
			}
			out << "\n";
		}
	}

	~Graph() {};

	size_t size;
	std::vector<std::vector<T> > matrix;
	std::vector<std::vector<T> > distance;
};

TEST(TestCase, Test1)
{
	int size = 3;
	Graph <int> graph(size);
	graph.matrix = { {0, 7, 77},
					{1, 0, 3},
					{4, 89, 0} };
	std::vector<std::vector<int>> actual = { {0, 7, 10},
											{1, 0, 3},
											{4, 11, 0} };
	graph.Floyd();

	for (int row = 0; row < size; ++row) {
		for (int col = 0; col < size; ++col) {
			ASSERT_EQ(graph.distance[row][col], actual[row][col]);
		}
	}
}


TEST(TestCase, Test2)
{
	int size = 4;
	Graph <int> graph(size);
	graph.matrix = { {0, 5, 9, 100},
					 {100, 0, 2, 8},
					 {100, 100, 0, 7 },
					 {4, 100, 100, 0 }};
	std::vector<std::vector<int>> actual = {{0, 5, 7, 13}, 
											{12, 0, 2, 8}, 
											{11, 16, 0, 7},
											{4, 9, 11, 0}};
	graph.Floyd();

	for (int row = 0; row < size; ++row) {
		for (int col = 0; col < size; ++col) {
			ASSERT_EQ(graph.distance[row][col], actual[row][col]);
		}
	}
}

int main(int argc, char **argv) {
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}