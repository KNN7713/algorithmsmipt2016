#include "numberDifferenceSubstrings.h"
#include <gtest\gtest.h>
#include <string>

TEST(TestCase, Test1)
{
  std::string str = "abab";
  str += '#';
  String string(str);
  string.suffArrayBuilder();
  string.buildLCPKasai();
  ASSERT_EQ(string.getDifferentSubstringsNumber(), 7);
}

TEST(TestCase, Test2)
{
  std::string str = "aaaaa";
  str += '#';
  String string(str);
  string.suffArrayBuilder();
  string.buildLCPKasai();
  ASSERT_EQ(string.getDifferentSubstringsNumber(), 5);
}

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}