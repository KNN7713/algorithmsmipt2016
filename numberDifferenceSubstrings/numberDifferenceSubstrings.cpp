#include "numberDifferenceSubstrings.h"

String::String(std::string _str) {
  str = _str;
  suffArray.resize(maxlen, 0);
  lcp.resize(maxlen, 0);
}

void String::suffArrayBuilder() {
  std::vector<int> count(maxlen, 0);
  std::vector<int> suffEqClss(maxlen);

  int length = str.length();

  for (int index = 0; index < length; ++index) {
    ++count[str[index]];
  }

  for (int index = 1; index < alphabetSize; ++index) {
    count[index] += count[index - 1];
  }

  for (int index = 0; index< length; ++index) {
    suffArray[--count[str[index]]] = index;
  }

  suffEqClss[suffArray[0]] = 0;
  int classes = 1;
  for (int index = 1; index < length; ++index) {
    if (str[suffArray[index]] != str[suffArray[index - 1]]) {
      ++classes;
    }
    suffEqClss[suffArray[index]] = classes - 1;
  }

  std::vector<int> suffArrayForNIteration(maxlen);
  std::vector<int> suffEqClssN(maxlen);

  for (int step = 0; (1 << step) < length; ++step) {
    for (int index = 0; index < length; ++index) {
      suffArrayForNIteration[index] = suffArray[index] - (1 << step);
      if (suffArrayForNIteration[index] < 0) {
        suffArrayForNIteration[index] += length;
      }
    }

    for (int index = 0; index < classes; ++index) {
      count[index] = 0;
    }

    for (int index = 0; index < length; ++index) {
      ++count[suffEqClss[suffArrayForNIteration[index]]];
    }

    for (int index = 1; index < classes; ++index) {
      count[index] += count[index - 1];
    }

    for (int index = length - 1; index >= 0; --index) {
      suffArray[--count[suffEqClss[suffArrayForNIteration[index]]]] = suffArrayForNIteration[index];
    }

    suffEqClssN[suffArray[0]] = 0;
    classes = 1;

    int curMiddle = 0;
    int prevMiddle = 0;
    for (int index = 1; index < length; ++index) {
      curMiddle = (suffArray[index] + (1 << step)) % length;
      prevMiddle = (suffArray[index - 1] + (1 << step)) % length;
      if (suffEqClss[suffArray[index]] != suffEqClss[suffArray[index - 1]] ||
        suffEqClss[curMiddle] != suffEqClss[prevMiddle]) {
        ++classes;
      }
      suffEqClssN[suffArray[index]] = classes - 1;
    }

    for (int index = 0; index < maxlen; ++index) {
      suffEqClss[index] = suffEqClssN[index];
    }
  }
  std::vector<int> answer;
  for (int index = 0; index < length; ++index) {
    answer.push_back(suffArray[index]);
  }
  suffArray.resize(answer.size());
  suffArray = answer;
}

void String::buildLCPKasai() {
  int length = str.length();
  std::vector<int> converse(length);
  for (int index = 0; index < length; ++index) {
    converse[suffArray[index]] = index;
  }
  int position = 0;
  for (int index = 0; index < length; ++index) {
    if (position > 0) {
      --position;
    }
    if (converse[index] == length - 1)
    {
      lcp[length - 1] = -1;
      position = 0;
    }
    else {
      int nextSuff = suffArray[converse[index] + 1];
      while (std::max(index + position, nextSuff + position) < length &&
        str[index + position] == str[nextSuff + position]) {
        ++position;
      }
      lcp[converse[index]] = position;
    }
  }
}

int String::getDifferentSubstringsNumber() {
  int answer = 0;
  int size = suffArray.size();
  for (int index = 1; index < size; ++index) {
    answer += size - suffArray[index] - 1;
  }
  for (int index = 1; index < lcp.size() - 1; ++index) {
    answer -= lcp[index];
  }
  return --answer;
}
