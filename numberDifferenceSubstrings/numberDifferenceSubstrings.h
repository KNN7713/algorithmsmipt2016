#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

const int maxlen = 1000;
const int alphabetSize = 256;

class String {
public:

  String(std::string _str);

  void suffArrayBuilder();

  void buildLCPKasai();

  int getDifferentSubstringsNumber();
private:
  std::string str;
  std::vector<int> suffArray;
  std::vector<int> lcp;
};