#include "numberDifferenceSubstrings.h"

int main() {
  std::string str;
  std::cin >> str;
  str += '#';
  String string(str);
  string.suffArrayBuilder();
  string.buildLCPKasai();
  std::cout << string.getDifferentSubstringsNumber();
  return 0;
}