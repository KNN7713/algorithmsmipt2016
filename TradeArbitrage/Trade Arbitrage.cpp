#include "TradeArbitrage.h"

Graph::Graph(size_t size) {
		
  ExchangeCosts_.resize(size);
  arbitration.resize(size);

  for (size_t vertex = 0; vertex < size; ++vertex) {
    ExchangeCosts_[vertex].resize(size);
  }

  for (size_t ribs_start = 0; ribs_start < size; ++ribs_start) {
	for (size_t ribs_end = 0; ribs_end < size; ++ribs_end) {
	  ExchangeCosts_[ribs_start][ribs_end] = 0;
	}
  }
};

Graph Graph::make_graph_from_stream(std::istream& input) {
  Graph graph(ExchangeCosts_.size());
  for (size_t ribs_start = 0; ribs_start < graph.ExchangeCosts_.size(); ++ribs_start) {
	for (size_t ribs_end = 0; ribs_end < graph.ExchangeCosts_.size(); ++ribs_end) {
	  if (ribs_start == ribs_end) {
		graph.ExchangeCosts_[ribs_start][ribs_end] = 1;
	  }
	  else {
		input >> graph.ExchangeCosts_[ribs_start][ribs_end];
	  }
	  if (graph.ExchangeCosts_[ribs_start][ribs_end] == -1) {
		graph.ExchangeCosts_[ribs_start][ribs_end] = INF;
	  }
	}
  }
  return graph;
}

bool Graph::FindArbitration() {
  for (size_t vertex = 0; vertex < ExchangeCosts_.size(); ++vertex) {
	if (!FordBellman(vertex)) {
	  return true;
	}
  }
  return false;
}

bool Graph::FordBellman(double vertex) {
  for (size_t curVertex = 0; curVertex < ExchangeCosts_.size(); ++curVertex) {
	arbitration[curVertex] = INF;
  }
  arbitration[vertex] = 1.0;
  bool relaxed = false;
		
  for (size_t stage = 1; stage < ExchangeCosts_.size() - 1; ++stage) {
	for (size_t ribs_start = 0; ribs_start < ExchangeCosts_.size(); ++ribs_start) {
	  for (size_t ribs_end = 0; ribs_end < ExchangeCosts_.size(); ++ribs_end) {
		if (ExchangeCosts_[ribs_start][ribs_end] != INF && arbitration[ribs_end] < 
            arbitration[ribs_start] * ExchangeCosts_[ribs_start][ribs_end]) {
		  arbitration[ribs_end] = arbitration[ribs_start] * ExchangeCosts_[ribs_start][ribs_end];
		  relaxed = true;
		}
	  }
	}
	if (!relaxed) {
	  break;
	}
  }
  if (arbitration[vertex] > 1.0) {
	return false;
  }
  return true;
};
	
void Graph::PrintExchangeCosts() {
  for (size_t ribs_start = 0; ribs_start < ExchangeCosts_.size(); ++ribs_start) {
	for (size_t ribs_end = 0; ribs_end < ExchangeCosts_.size(); ++ribs_end) {
	  std::cout << ExchangeCosts_[ribs_start][ribs_end] << " ";
	}
	std::cout << std::endl;
  }
};

Graph::~Graph() {};