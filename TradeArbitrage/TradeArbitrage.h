#include <iostream>
#include <fstream>
#include <vector>
#include <limits.h>

const int INF = -INT_MAX;

class Graph {
public:

  Graph(size_t size);

  Graph make_graph_from_stream(std::istream& input);

  bool FindArbitration();

  bool FordBellman(double vertex);

  void PrintExchangeCosts();

  ~Graph();

  std::vector<std::vector<double> > ExchangeCosts_;
private:
  std::vector<double> arbitration;
};