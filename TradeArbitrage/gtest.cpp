#include "TradeArbitrage.h"
#include <gtest\gtest.h>
#include <string>

TEST(TestCase, Test1)
{
  size_t size = 3;

  Graph graph(size);
  graph.ExchangeCosts_ = {{1.0, -1.0, 2.0},
                          {3.0, 1.0, -1.0},
                          {-1.0, 4.0, 1.0} };
  
  std::string actual;
  if (graph.FindArbitration()) {
    actual = "YES";
  }
  else {
    actual = "NO";
  }
  ASSERT_EQ(actual, "NO");
}

TEST(TestCase, Test2)
{
  size_t size = 5;

  Graph graph(size);
  graph.ExchangeCosts_ = {{ 1.0, 1.0, 4.0, 1.0, 5.0 },
                          { 9.0, 1.0, 2.0, 6.0, 5.0 },
                          { 3.0, 5.0, 1.0, 8.0, 9.0 },
                          { 7.0, 9.0, 3.0, 1.0, 2.0 },
                          { 3.0, 8.0, 4.0, 3.0, 1.0} };

  std::string actual;
  if (graph.FindArbitration()) {
    actual = "YES";
  }
  else {
    actual = "NO";
  }
  ASSERT_EQ(actual, "YES");
}

int main(int argc, char** argv)
{
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
