#include "TradeArbitrage.h" 

int main() {
  size_t size;
  std::cin >> size;

  Graph graph(size);
  graph = graph.make_graph_from_stream(std::cin);
  if (graph.FindArbitration()) {
    std::cout << "YES";
  }
  else {
    std::cout << "NO";
  }
  return 0;
}