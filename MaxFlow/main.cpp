#include "SearchMaxFlow.h"

int main() {
  int vertexNumber, edgeNumber;
  std::cin >> vertexNumber;
  int source, target;
  while (vertexNumber != 0) {
    std::cin >> source >> target >> edgeNumber;
    std::vector<std::vector<type>> matrix(vertexNumber + 1);
    int startVertex, endVertex, curCapacity;
    for (int i = 0; i < vertexNumber; ++i) {
      matrix[i].resize(vertexNumber, { 0,0 });
    }
    for (int curEdge = 0; curEdge < edgeNumber; ++curEdge) {
      std::cin >> startVertex >> endVertex >> curCapacity;
      matrix[startVertex - 1][endVertex - 1].capacity += curCapacity;
      matrix[endVertex - 1][startVertex - 1].capacity += curCapacity;
    }
    Graph graph(vertexNumber, source, target, edgeNumber, matrix);
    std::cout << graph.getMaxFlow() << std::endl;
    std::cin >> vertexNumber;
  }
  return 0;
}