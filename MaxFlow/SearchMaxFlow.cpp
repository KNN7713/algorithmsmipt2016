#include "SearchMaxFlow.h"

Graph::Graph(int size, int source_, int target_, int edgeNumber_, std::vector<std::vector<type>>& graph_) {
  edgeNumber = edgeNumber_;
  target = target_ - 1;
  source = source_ - 1;
  vertexNumber = size;
  matrix.resize(size + 1);
  for (int vertex = 0; vertex < vertexNumber; ++vertex) {
    matrix[vertex].resize(vertexNumber, { 0, 0 });
  }
  matrix = graph_;
};

int Graph::findIncreasePathValue(int curVertex) {
  visited[curVertex] = true;
    
  if (curVertex == target) {
    return result;
  }

  int curCapacity;
  for (int secondVertex = 0; secondVertex < matrix[curVertex].size(); ++secondVertex) {
    if (matrix[curVertex][secondVertex].capacity > matrix[curVertex][secondVertex].flow &&
      !visited[secondVertex]) {

      result = std::min(matrix[curVertex][secondVertex].capacity - matrix[curVertex][secondVertex].flow, result);
      curCapacity = findIncreasePathValue(secondVertex);

      if (curCapacity > 0) {
        result = std::min(result, curCapacity);
        matrix[curVertex][secondVertex].flow += result;
        matrix[secondVertex][curVertex].flow = -matrix[curVertex][secondVertex].flow;
        return result;
      }
    }
  }
  return 0;
}

int Graph::getMaxFlow() {
  bool isPathExist = true;
  int maxFlow = 0;
  while (isPathExist) {
    visited.assign(vertexNumber, false);
    result = INF;
    int currentFlow = findIncreasePathValue(source);
    maxFlow += currentFlow;
    if (currentFlow == 0) {
      isPathExist = false;
    }
  }
  return maxFlow;
}

Graph::~Graph(){}