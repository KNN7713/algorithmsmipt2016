#include "SearchMaxFlow.h"
#include <gtest\gtest.h>

TEST(TestCase, Test1)
{
  int vertexNumber = 3;
  int source = 1;
  int target = 2;
  int edgeNumber = 2;

  std::vector<std::vector<type>> matrix = { { {0, 0}, {0, 0}, {4, 0} },
                                            { {0, 0}, {0, 0}, {8, 0} }, 
                                            { {4, 0}, {8, 0}, {0, 0} }};
  Graph graph(vertexNumber, source, target, edgeNumber, matrix);
  ASSERT_EQ(graph.getMaxFlow(), 4);
}

TEST(TestCase, Test2)
{
  int vertexNumber = 5;
  int source = 1;
  int target = 5;
  int edgeNumber = 10;

  std::vector<std::vector<type>> matrix = { { { 0, 0 }, { 3, 0 }, { 5, 0 }, { 9, 0 }, { 16, 0 } },
                                            { { 3, 0 }, { 0, 0 }, { 7, 0 }, { 13, 0 }, { 25, 0 } },
                                            { { 5, 0 }, { 7, 0 }, { 0, 0 }, { 34, 0 }, { 8, 0 } },
                                            { { 9, 0 }, { 13, 0 }, { 34, 0 }, { 0, 0 }, { 9, 0 } },
                                            { { 16, 0 }, { 25, 0 }, { 8, 0 }, { 9, 0 }, { 0, 0 } } };
  Graph graph(vertexNumber, source, target, edgeNumber, matrix);
  ASSERT_EQ(graph.getMaxFlow(), 33);
}

int main(int argc, char** argv)
{
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}