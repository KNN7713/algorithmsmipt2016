#include <iostream>
#include <vector>
#include <algorithm>
#include <limits>

const int INF = INT_MAX;

struct type {
  int capacity;
  int flow;
};


class Graph {
public:
  Graph(int size, int source_, int target_, int edgeNumber_, std::vector<std::vector<type>>& graph_);

  int findIncreasePathValue(int curVertex);

  int getMaxFlow();

  ~Graph();
private:
  std::vector<bool> visited;
  std::vector<std::vector<type>> matrix;
  int result;
  int source, target;
  int vertexNumber, edgeNumber;
};